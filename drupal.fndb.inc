<?php
//  FNSave - abstract class to storing annotations
class FNSave {

  function FNSave() { }
  
  function saveNewAnnotation() { }
  
  function updateExistingAnnotationByID() { }
  
  function deleteAnnotationByID() { }
}


//  FNSaveDatabaseRows - concrete class to save in database, annotations stored in separate rows
class FNSaveDatabaseRows extends FNSave {
  
  function FNSaveDatabaseRows() {
  }
  
  function saveNewAnnotation($fn_image, $fn_annotation) {
    global $user;
    $result = db_query("INSERT INTO {imagenotes} (nid, uid, annotation_id, title, content, xml) VALUES (%d, %d, '%s', '%s', '%s', '%s')",
                       $fn_image->param['nid'], $user->uid, $fn_annotation->param['id'], $fn_annotation->param['title'], $fn_annotation->param['content'], $fn_annotation->param['src_xml']);
    return true;
  }
	
  function updateExistingAnnotationByID($fn_image, $fn_annotation) {
    $result = db_query("UPDATE {imagenotes} SET title='%s', content='%s', xml='%s' WHERE annotation_id='%s'",
                       $fn_annotation->param['title'], $fn_annotation->param['content'], $fn_annotation->param['src_xml'], $fn_annotation->param['id']);
    return true;
  }
	
  function deleteAnnotationByID($fn_image, $fn_annotation) {
    $result = db_query("DELETE FROM {imagenotes} WHERE annotation_id = '%s'", $fn_annotation->param['id']);
    return true;	
  }
}

//  FNRetrieve - abstract class to retrieve entries
class FNRetrieve {

	function FNRetrieve() { }

	function getAnnotations() { }
	
	function getAnnotationsByID() { }
}

// FNRetrieveDatabaseRows - concrete class to fetch from database, annotations stored in separate rows
class FNRetrieveDatabaseRows extends FNRetrieve {

	function FNRetrieveDatabaseRows() {
	}
		
	function getAnnotations(&$fn_image) {
      global $DHTML_MAXWIDTH, $DHTML_MAXHEIGHT;

      $node = node_load($fn_image->param['nid']);
      $this->image = file_create_path($node->images['preview']);

      $size = getimagesize($this->image);
      displayDebugParam($size, 4);
      $ratioWidth = $DHTML_MAXWIDTH /  $size[0];
      $ratioHeight = $DHTML_MAXHEIGHT / $size[1];
      
      if($ratioHeight>$ratioWidth){$ratio=$ratioWidth;}else{$ratio=$ratioHeight;}
      if($ratio>1){$ratio=1;}
      
      $fn_image->setFnImageParam('scalefactor', $ratio);
      
      $annoatations = array();
      $xml = '';
      $result = db_query("SELECT * FROM {imagenotes} WHERE nid=%d", arg(1));
      while ($row = db_fetch_object($result)) {
        $entry = $row->xml;
        preg_match("#<fn:boundingBox>(.*)</fn:boundingBox>#Umsi", $entry, $coordstring);
        $coords = explode(",", $coordstring[1]);
        list(	$annotation['upperleftx'], 
                $annotation['upperlefty'], 
                $annotation['lowerrightx'], 
                $annotation['lowerrighty'])  = $coords;
        $annotation['width'] = ($coords[2] - $coords[0])*$ratio;
        $annotation['width'] = ($coords[2] - $coords[0])*$ratio;
        $annotation['height'] = ($coords[3] - $coords[1])*$ratio;
        $annotation['upperlefty'] *= $ratio;
        $annotation['upperleftx'] *= $ratio;
        $annotation['lowerrightx'] *= $ratio;
        $annotation['lowerrighty'] *= $ratio;
			
        preg_match("#<title>(.*)</title>#Umsi", $entry, $title);
        $annotation['title'] = $title[1];
        preg_match("#<content.*>(.*)</content>#Umsi", $entry, $content);
        $annotation['content'] = $content[1];
        preg_match("#<name>(.*)</name>#Umsi", $entry, $author);
        $annotation['author'] = $author[1];
        preg_match("#<created>(.*)</created>#Umsi", $entry, $created);
        $annotation['created'] = $created[1];
        preg_match("#<issued>(.*)</issued>#Umsi", $entry, $issued);
        $annotation['issued'] = $issued[1];
        preg_match("#<modified>(.*)</modified>#Umsi", $entry, $modified);
        $annotation['modified'] = $modified[1];
        preg_match("#<id>(.*)</id>#Umsi", $entry, $id);
        //$annotation['id'] = basename($id[1]);  Do not get basename, use full url
        $annotation['id'] = $id[1];
        preg_match("#<userid>(.*)</userid>#Umsi", $entry, $userid);
        $annotation['userid'] = $userid[1];
        $annotations[] = $annotation;
      }
      return $annotations;
	}
	
	function getAnnotationsByID() {	
	}		
}
