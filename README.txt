Imagenotes Module

This module provides "flickr - style" note capabilities for image nodes.

Requires the JS & PHP libraries from http://fotonotes.net/

See INSTALL.txt for installation instructions


Questions, features, etc: James Walker <walkah@walkah.net>
